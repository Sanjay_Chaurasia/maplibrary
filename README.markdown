MapKit
======

MapKit is an iOS 'library' for displaying tiled maps. While `MKMapView` was introduced in iOS 3.0, it lacks support for tile providers other than Google Maps. While this is not a big issue for most, it prevents developers from building applications that utilize data from atypical sources such as Google Mars or Google Sky. Please be aware of licensing issues when building custom tile providers. 

NOTE: MapKit was only tested on iOS 4.0. Due to changes in thread-safety of UIKit, it may not function properly on earlier versions of iOS.

Usage
-----

To use MapKit in your project, simply drag the contents of the `Sources` folder into your Xcode project.

Add the Mapkit Framwork from Linkded frameworks and Libray from General option
import Mapkit on the viewcontroller on which you have create you MapView instance
Use "LocationDelegate" Protocol for getting the current latitude and longitude geo-cordinates vales.

How to call location update
----------------------------------------------
var obj : UserLocation? = nil
Create instance of UserLocation by passing desired accuracy and distancefilter
obj = UserLocation(locationAccuracy: kCLLocationAccuracyBest,distance: kCLDistanceFilterNone)
obj?.delegate = self
obj?.mapDelegate = self
obj!.startUpdatingMyLocation()

overide the protocol method method

func locationtrackDidUpdate(currentLocation:CLLocation){
print("coordinates updates = current latitude \(currentLocation.coordinate.latitude) current longitude \(currentLocation.coordinate.longitude)")

}
func locationtrackDidFailed(error:NSError){

}


How to use  latitude and longitude values
------------------------------------------
let latitude = obj!.mylocation!.location?.coordinate.latitude
let longitude = obj!.mylocation!.location?.coordinate.longitude 

How to call addAnotation
------------------------------------------
After getting the array of annotations call the method 
obj?.addAnotation(self, annotationArray: array as! NSArray ,pinTypes: PinTypes.DefaultPin)

//Use various pintypes which is enum based on requirement

After calling addAnotation make sure you use AnnotaionProtocal and overide below method 
------------------------------------------
// MARK: Conform to protocol AnnotaionProtocal
func addAnnotionFromArray(mapView:MKMapView,array:NSArray)
{

}
how to stopupdate location
------------------------------------------
obj!.stopUpdatingMyLocation()


how to use thumbnail  annotions
------------------------------------------
About : Thubnail annoation will have image as pin say any location image and have expand and shrink animation
It will belong to ThumbPin which is enum type PinTypes.

In ViewforAnnotation map delegate code will be like below

case .ThumbPin :
annotationView = ThumbnailAnnotationView(identifire: identifier, annotation: an)
return annotationView

------------------------------------------
for conforming didselect(expand) and didDeselect(shrink animation) methods

func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {

guard let customView = view as? ThumbnailAnnotationView else {
// doesn't match
return
}
customView.didSelectAnnotationViewInMap(mapView)


}

func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
guard let customView = view as? ThumbnailAnnotationView else {
// doesn't match
return
}
customView.didDeSelectAnnotationViewInMap(mapView)
}


