//
//  ThumbnailAnnotation.swift
//  MyLocation
//
//  Created by Sanjay on 13/05/16.
//  Copyright © 2016 Sanjay. All rights reserved.
//

import Foundation
import MapKit

let kThumbnailAnnotationViewStandardWidth     = 75.0
let kThumbnailAnnotationViewStandardHeight    = 87.0
let kThumbnailAnnotationViewExpandOffset      = 200.0
let kThumbnailAnnotationViewVerticalOffset : CGFloat    = 34.0 //Positive offset values move the annotation view down and to the right, while negative values move it up and to the left.
let kThumbnailAnnotationViewAnimationDuration = 0.25
protocol ThumbnailAnnotationViewProtocol{
    
    func didSelectAnnotationViewInMap(mapView:MKMapView)
    func didDeSelectAnnotationViewInMap(mapView:MKMapView)
    
}
protocol SubViewProtocol {
     var imageView: UIImageView? { get }
     var titleLabel: UILabel? { get }
     var subtitleLabel: UILabel? { get }
     var disclousreButton : UIButton? {get}
     var bgLayer :  CAShapeLayer? {get}
   
     //var coordinate : CLLocationCoordinate2D {get}
}
enum ThumbnailAnnotationViewAnimationDirection {
    case  DirectionGrow, DirectionShrink
}
enum  ThumbnailAnnotationViewState {
    case Collapsed, Expanded,Animating
}

class  ThumbnailAnnotationView: MKAnnotationView , SubViewProtocol , ThumbnailAnnotationViewProtocol{
    
    var myannotation: MyAnnotion?
    var myreuseIdentifier: String?
    var state : ThumbnailAnnotationViewState?
    var imageView : UIImageView?
    var titleLabel: UILabel?
    var subtitleLabel: UILabel?
    var disclousreButton : UIButton?
    var coordinate: CLLocationCoordinate2D?
     var bgLayer :  CAShapeLayer?
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    
    required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }
    init(identifire:String , annotation:MyAnnotion)
    { self.myannotation = annotation
        super.init(annotation: self.myannotation, reuseIdentifier: myreuseIdentifier)
      
        self.canShowCallout = false
        self.frame = CGRectMake(0, 0, CGFloat( kThumbnailAnnotationViewStandardWidth),  CGFloat(kThumbnailAnnotationViewStandardHeight))
        self.backgroundColor = UIColor.clearColor();
        self.centerOffset = CGPointMake(0, -kThumbnailAnnotationViewVerticalOffset)
        self.state = ThumbnailAnnotationViewState.Collapsed
        self.myreuseIdentifier = identifire
        self.myannotation = annotation
        self.setupView()
        self.updateWithThumbnail(self.myannotation!)
        
        
       
    }
    
    func setupView()  {
        self.setupImageView()
        self.setupTitleLabel()
        self.setsubtitleLabel()
        self.setupDisclosureButton()
        self.setLayerProperties()
         self.setDetailGroupAlpha(0.0)
        
    }
    func setupImageView(){
        self.imageView = UIImageView(frame: CGRectMake(12.5, 12.5, 50.0, 47.0))
        self.imageView?.layer.cornerRadius = 4.0
        self.imageView?.layer.masksToBounds = true
        self.imageView?.layer.borderColor = UIColor .lightGrayColor().CGColor
        self.imageView?.layer.borderWidth = 0.5
        self.addSubview(self.imageView!)
    }
    func  setupTitleLabel(){
        self.titleLabel = UILabel(frame: CGRectMake(-32.0, 16.0, 168.0, 20.0))
        self.titleLabel?.textColor = UIColor.darkGrayColor()
         self.titleLabel?.font = UIFont.boldSystemFontOfSize(17)
         self.titleLabel?.minimumScaleFactor = 0.8
         self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.addSubview(self.titleLabel!)
    }
    
    func setsubtitleLabel(){
        self.subtitleLabel = UILabel(frame :CGRectMake(-32.0, 36.0, 168.0, 20.0));
         self.subtitleLabel?.textColor = UIColor.grayColor()
        self.subtitleLabel?.font = UIFont.systemFontOfSize(12);
        self.addSubview(self.subtitleLabel!);
    }
    
    func setupDisclosureButton(){
       let iOS7 =  Double(UIDevice.currentDevice().systemVersion) >= 7.0
       let buttonType:UIButtonType = iOS7 ? UIButtonType.System : UIButtonType.Custom
       self.disclousreButton = UIButton(type: buttonType)
       self.disclousreButton?.tintColor = UIColor.grayColor()
       let disclosureIndicatorImage = ThumbnailAnnotationView.disclosureButtonImage()
       self.disclousreButton?.setImage(disclosureIndicatorImage, forState: UIControlState.Normal)
        self.disclousreButton?.frame = CGRectMake( CGFloat(kThumbnailAnnotationViewExpandOffset/2.0) + self.frame.size.width/2.0 as CGFloat + 8.0,
                                                  26.5,
         
                                                  disclosureIndicatorImage.size.width as CGFloat,
                                                  disclosureIndicatorImage.size.height as CGFloat)
        self.disclousreButton!.addTarget(self, action:#selector(self.didTapDisclosureButton), forControlEvents: .TouchUpInside)
        self.addSubview(self.disclousreButton!)
        
    }
    
    func setLayerProperties ()  {
        
        self.bgLayer = CAShapeLayer()
        let path: CGPathRef = self.newBubbleWithRect(self.bounds)
        self.bgLayer?.path = path
        self.bgLayer?.fillColor = UIColor.whiteColor().CGColor
        self.bgLayer?.shadowColor = UIColor.blueColor().CGColor
        self.bgLayer?.shadowOffset = CGSizeMake(0.0, 3.0)
        self.bgLayer?.shadowRadius = 2.0
        self.bgLayer?.shadowOpacity = 0.5
        self.bgLayer?.masksToBounds = false
        self.layer.insertSublayer(self.bgLayer!, atIndex: 0)
      
}
    class func disclosureButtonImage()-> UIImage {
    
        let size:CGSize = CGSizeMake(21.0, 36.0)
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.mainScreen().scale)
        let bezierPath = UIBezierPath()
        bezierPath.moveToPoint(CGPointMake(2.0, 2.0))
        bezierPath.addLineToPoint(CGPointMake(10.0, 10.0))
        bezierPath.addLineToPoint(CGPointMake(2.0, 18.0))
        UIColor.lightGrayColor().setStroke()
        bezierPath.lineWidth = 3.0
        bezierPath.stroke()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image
        
    }
    
    func didTapDisclosureButton()   {
        //Annotation tapped
    }
    
    func updateWithThumbnail(thumbnail :MyAnnotion ) {
    self.coordinate = thumbnail.coordinate;
    self.titleLabel!.text = thumbnail.title;
    self.subtitleLabel!.text = thumbnail.subtitle;
    self.imageView!.image = UIImage(named: "empire.png")
    }

    
    //MARK: thumbnailannotation protoco
    func didSelectAnnotationViewInMap(mapView:MKMapView){
        // Center map at annotation point
        mapView.setCenterCoordinate(self.coordinate!, animated: true)
     
        self.expand()
    }
    func didDeSelectAnnotationViewInMap(mapView:MKMapView){
        self.shrink()
    }
    
    func setDetailGroupAlpha(alpha:CGFloat)  {
        self.disclousreButton?.alpha = alpha;
        self.titleLabel?.alpha = alpha;
        self.subtitleLabel?.alpha = alpha;
    }
    
    //MARK: expand
    func expand(){
        
        if self.state != ThumbnailAnnotationViewState.Collapsed {
            return
        }
        self.state = ThumbnailAnnotationViewState.Animating
        self.animateBubbleWithDirection(ThumbnailAnnotationViewAnimationDirection.DirectionGrow)
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width+CGFloat( kThumbnailAnnotationViewExpandOffset), self.frame.size.height)
        self.centerOffset =  CGPointMake(CGFloat(kThumbnailAnnotationViewExpandOffset)/2.0, CGFloat(-kThumbnailAnnotationViewVerticalOffset))
        UIView.animateWithDuration(kThumbnailAnnotationViewAnimationDuration/2.0, delay: kThumbnailAnnotationViewAnimationDuration, options:UIViewAnimationOptions.CurveEaseInOut, animations: {()-> Void in
               self.setDetailGroupAlpha(1.0)
            
            }, completion: { (finished: Bool) -> Void in
                self.state = ThumbnailAnnotationViewState.Expanded
        })
    }
    
    
    //MARK: shrink
    func shrink()  {
        if self.state != ThumbnailAnnotationViewState.Expanded {
            return
        }
        self.state = ThumbnailAnnotationViewState.Animating
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width-CGFloat( kThumbnailAnnotationViewExpandOffset), self.frame.size.height)
        UIView.animateWithDuration(kThumbnailAnnotationViewAnimationDuration/2.0, delay: 0.0, options:UIViewAnimationOptions.CurveEaseInOut, animations: {()-> Void in
            self.setDetailGroupAlpha(0.0)
            
            }, completion: { (finished: Bool) -> Void in
               self.animateBubbleWithDirection(ThumbnailAnnotationViewAnimationDirection.DirectionShrink)
               self.centerOffset =  CGPointMake(CGFloat(0.0), CGFloat(-kThumbnailAnnotationViewVerticalOffset))
        })
    }
    
    func animateBubbleWithDirection(animationDirection:ThumbnailAnnotationViewAnimationDirection) {
        
        let growing : Bool = (animationDirection == ThumbnailAnnotationViewAnimationDirection.DirectionGrow)
        UIView.animateWithDuration(kThumbnailAnnotationViewAnimationDuration, animations: { () -> Void in
            let xOffset:CGFloat = (growing ? -1 : 1) * CGFloat( kThumbnailAnnotationViewExpandOffset/2.0)
            self.imageView!.frame = CGRectOffset(self.imageView!.frame, xOffset, 0.0);
            
            }, completion: { (finished: Bool) -> Void in
                if(animationDirection == ThumbnailAnnotationViewAnimationDirection.DirectionShrink){
                    self.state = ThumbnailAnnotationViewState.Collapsed
                }
                
                
        })
        
        // Bubble
        let animation = CABasicAnimation(keyPath: "path")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.repeatCount = 1
        animation.removedOnCompletion = false
        animation.fillMode = kCAFillModeForwards
        animation.duration = kThumbnailAnnotationViewAnimationDuration
        
       // Stroke & Shadow From/To Values
        let largeRect: CGRect  = CGRectInset(self.bounds,CGFloat( -kThumbnailAnnotationViewExpandOffset/2.0), 0.0);
        let fromPath : CGPathRef = self.newBubbleWithRect(growing ? self.bounds : largeRect)
       
        animation.fromValue = fromPath;
        let toPath : CGPathRef  = self.newBubbleWithRect(growing ? largeRect : self.bounds)
        animation.toValue = toPath;
       
        self.bgLayer?.addAnimation(animation, forKey: animation.keyPath)
    }
    
    func newBubbleWithRect(rect:CGRect) -> CGPathRef {
        var myrect = rect
        
        let stroke:CGFloat  = 1.0;
        let radius:CGFloat  = 7.0;
        let  path:CGMutablePathRef = CGPathCreateMutable();
        let  parentX:CGFloat = rect.origin.x + rect.size.width/2.0;
        
        // Determine Size
        myrect.size.width -= stroke + 14.0;
        myrect.size.height -= stroke + 29.0;
        myrect.origin.x += stroke / 2.0 + 7.0;
        myrect.origin.y += stroke / 2.0 + 7.0;
        
        // Create Callout Bubble Path
        CGPathMoveToPoint(path, nil, myrect.origin.x, myrect.origin.y + radius)
        CGPathAddLineToPoint(path, nil, myrect.origin.x, myrect.origin.y + myrect.size.height - radius);
        CGPathAddArc(path, nil, myrect.origin.x + radius, myrect.origin.y + myrect.size.height - radius, radius, CGFloat( M_PI),CGFloat( M_PI_2), true);
        CGPathAddLineToPoint(path, nil, parentX - 14.0, myrect.origin.y + myrect.size.height);
        CGPathAddLineToPoint(path, nil, parentX, myrect.origin.y + myrect.size.height + 14.0);
        CGPathAddLineToPoint(path, nil, parentX + 14.0, myrect.origin.y + myrect.size.height);
        CGPathAddLineToPoint(path, nil, myrect.origin.x + myrect.size.width - radius, myrect.origin.y + myrect.size.height);
        CGPathAddArc(path, nil, myrect.origin.x + myrect.size.width - radius, myrect.origin.y + myrect.size.height - radius, radius, CGFloat(M_PI_2), 0.0, true);
        CGPathAddLineToPoint(path, nil, myrect.origin.x + myrect.size.width, myrect.origin.y + radius);
        CGPathAddArc(path, nil, myrect.origin.x + myrect.size.width - radius, myrect.origin.y + radius, radius, 0.0, CGFloat(-M_PI_2), true);
        CGPathAddLineToPoint(path, nil, myrect.origin.x + radius, myrect.origin.y);
        CGPathAddArc(path, nil, myrect.origin.x + radius, myrect.origin.y + radius, radius, CGFloat(-M_PI_2), CGFloat(M_PI), true);
        CGPathCloseSubpath(path);
        return path;
    }
   

}





