//
//  ViewController.swift
//  MyLocation
//
//  Created by Sanjay on 06/05/16.
//  Copyright © 2016 Sanjay. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class ViewController: UIViewController ,LocationDelegate,AnnotaionProtocal {

    let kGOOGLE_API_KEY : String = "AIzaSyBYG5F7gTMrgWEmcMjOzeHn5mbjCRT5ISs"
    var currenDist:CLLocationDistance = 0.0
    var currentCentre:CLLocationCoordinate2D?
    @IBOutlet weak var mapView: MKMapView!
    var pinsType = PinTypes.DefaultPin
    var buttonType = ButtonType.DetailDisclosure
    var obj : UserLocation? = nil
        override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        obj = UserLocation(locationAccuracy: kCLLocationAccuracyBest,distance: kCLDistanceFilterNone)
        obj?.delegate = self
        obj?.mapDelegate = self
        obj!.startUpdatingMyLocation()
    }
    @IBAction func switchAction(sender: AnyObject) {
         self.mapView?.removeOverlays((self.mapView?.overlays)!)
        if(sender.selectedSegmentIndex == 0)
        {
             pinsType = PinTypes.DefaultPin
             obj?.hasLeftAccesoryView = false
             obj?.hasRighAccesoryView = false
        }
        else if(sender.selectedSegmentIndex == 1){
             pinsType = PinTypes.CustomPin
             obj?.hasLeftAccesoryView = true
             obj?.hasRighAccesoryView = true
             buttonType = ButtonType.Custom
        }
        else if(sender.selectedSegmentIndex == 3){
            pinsType = PinTypes.PinWithLeftAccesorView
            obj?.hasLeftAccesoryView = true
             obj?.hasRighAccesoryView = false
        }
        else if(sender.selectedSegmentIndex == 4){
            pinsType = PinTypes.ThumbPin
            obj?.hasLeftAccesoryView = true
            obj?.hasRighAccesoryView = true
        }
        else
        {
            
            
            let source = MKPlacemark(coordinate: CLLocationCoordinate2DMake(37.776142, -122.424774), addressDictionary: ["":""])
            let srcMapItem = MKMapItem(placemark: source)
            srcMapItem.name = ""
            
            let destination = MKPlacemark(coordinate: CLLocationCoordinate2DMake(37.73787, -122.373962), addressDictionary: ["":""])
            let destinationItem = MKMapItem(placemark: destination)
            destinationItem.name = ""
            
            let directionRequest = MKDirectionsRequest()
            directionRequest.source = srcMapItem
            directionRequest.destination = destinationItem
            directionRequest.transportType = MKDirectionsTransportType.Walking
            let route =   MapRoute(Request: directionRequest, Obj: self, color: UIColor.blueColor(), width: 3.0)
            route.drawRoute()
        }
        self.searchByName("Bar")
       
    
    }
    @IBAction func refreshMap(sender: AnyObject) {
         //obj!.startUpdatingMyLocation()
          print("current latitude \(obj!.mylocation!.location?.coordinate.latitude) current longitude \(obj!.mylocation!.location?.coordinate.longitude)")
       
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func locationtrackDidUpdate(currentLocation:CLLocation){
         print("coordinates updates = current latitude \(currentLocation.coordinate.latitude) current longitude \(currentLocation.coordinate.longitude)")
     
    }
    func locationtrackDidFailed(error:NSError){
        
    }
        
    func searchByName(placeName: String){
        let url : NSString
        guard let latitude = obj!.mylocation!.location?.coordinate.latitude else{
            return}
        
        guard let longitude = obj!.mylocation!.location?.coordinate.longitude else{
            return
        }
        url = "https://maps.googleapis.com/maps/api/place/search/json?location=\(latitude),\(longitude) &radius=\(1000)&types=\(placeName)&key=\(kGOOGLE_API_KEY)"
      
        let requestUrl : NSString = url.stringByAddingPercentEncodingWithAllowedCharacters(.URLQueryAllowedCharacterSet())!
        let searchURL : NSURL = NSURL(string: requestUrl as String)!
        
        let task = NSURLSession.sharedSession().dataTaskWithURL(searchURL) { (data:NSData?, response: NSURLResponse?, error:NSError?) -> Void in
            
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as! [String:AnyObject]
                var dict : NSDictionary
                dict = json
                guard let array = dict["results"] else{
                    return
                }
                dispatch_async(dispatch_get_main_queue(), {
                   self.obj?.addAnotation(self, annotationArray: array as! NSArray ,pinTypes: self.pinsType ,btnType: self.buttonType)
                })
            }catch{
                print("Some error occured")
            }
            
        }
        // Call the resume() method to start the NSURLSession task
        task.resume()
    
    }
   
    func fetchedData(responseData:NSData)  {
        
        do {
            let anyObj = try NSJSONSerialization.JSONObjectWithData(responseData, options: []) as! [String:AnyObject]
            // use anyObj here
            var dict : NSDictionary
            dict = anyObj
            print(dict)
            guard let array = dict["results"] else{
                return
            }
           obj?.addAnotation(self, annotationArray: array as! NSArray ,pinTypes: pinsType, btnType: self.buttonType)
        } catch let error as NSError {
            print("json error: \(error.localizedDescription)")
        }
    }
    
    
    // MARK: Conform to protocol AnnotaionProtocal
    func addAnnotionFromArray(mapView:MKMapView,array:NSArray)
    {
        //Remove any existing custom annotations but not the user location blue dot.
        for annotations in mapView.annotations {
            if annotations .isKindOfClass(MyAnnotion) {
                [mapView.removeAnnotation(annotations)]
            }
        }
       
        //Loop through the array of places returned from the Google API.
        for (index, element) in array.enumerate() {
            print("Item \(index): \(element)")
            let place : NSDictionary = array[index] as! NSDictionary
            var name:String?
            var vicinity:String?
            var placeid:String?
            
            name = place.objectForKey("name") as? String
            vicinity = place.objectForKey("vicinity") as? String
            placeid = place.objectForKey("place_id") as? String
            
            var geo : NSDictionary?
            geo = place.objectForKey("geometry") as? NSDictionary
            
            var loc : NSDictionary?
            loc = geo?.objectForKey("location") as? NSDictionary
            
            let coordinates =  CLLocationCoordinate2D(latitude:(loc?.objectForKey("lat") as? Double)! , longitude: (loc?.objectForKey("lng") as? Double)!)
            
            let imageName : String = "empire"
            let leftAccesoryImageName : String = "dollar"
            let rightAccesoryImageName : String = "dollar"
            let annotation = MyAnnotion(title: name!, subtitle: vicinity!, coordinate:coordinates, placeId: placeid! , imageName: imageName ,leftAccesoryImageName: leftAccesoryImageName ,rightAccesoryImageName:rightAccesoryImageName)
            
            var annotationView:MKPinAnnotationView!
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "MyAnnotion")
            mapView.addAnnotation(annotationView.annotation!)
            
        }
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    
       deinit{
        obj!.stopUpdatingMyLocation()
    }

}

