//
//  File.swift
//  MyLocation
//
//  Created by Sanjay on 06/05/16.
//  Copyright © 2016 Sanjay. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol AnnotaionProtocal {
    func addAnnotionFromArray(mapView:MKMapView,array:NSArray)
   
}

protocol LocationDelegate {
    func locationtrackDidUpdate(currentLocation:CLLocation)
    func locationtrackDidFailed(error:NSError)
}
enum PinTypes {
    case DefaultPin , CustomPin ,PinWithLeftAccesorView ,ThumbPin
    
}
enum ButtonType {
    case DetailDisclosure , InfoLight , InfoDark , ContactAdd , Custom
}

protocol AnnotationFeaturesProtocol {
    var hasLeftAccesoryView : Bool {get}
    var hasRighAccesoryView : Bool {get}
    var ButtonKind :ButtonType {get}
}
class UserLocation : NSObject, CLLocationManagerDelegate, AnnotationFeaturesProtocol{
    
   var mapView : MKMapView?
   var mylocation: CLLocationManager?
   var currentLocation2d:CLLocationCoordinate2D?
   var error:NSError?
   var accuracy:CLLocationAccuracy?
   var distance:Double?
   var delegate : LocationDelegate?
   var mapDelegate : AnnotaionProtocal?
   var pinsType = PinTypes.DefaultPin
   var ButtonKind: ButtonType = ButtonType.DetailDisclosure
   var hasLeftAccesoryView: Bool = false
   var hasRighAccesoryView : Bool = false
  init(locationAccuracy:CLLocationAccuracy , distance:Double) {
        super.init()
        self.mylocation = CLLocationManager()
        self.accuracy = locationAccuracy
        self.mylocation!.delegate = self
        self.distance = distance
        if self.mylocation!.respondsToSelector(#selector(CLLocationManager.requestAlwaysAuthorization)) {
           // self.mylocation!.requestWhenInUseAuthorization()
            self.mylocation!.requestAlwaysAuthorization()
        }
        self.mylocation!.desiredAccuracy = self.accuracy!
        
        self.mylocation!.distanceFilter = distance
    
    }
    
    //MARK: conform to CLLocationManagerDelegate for didUpdateLocations
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
       guard let location = locations.last else {
            return
        }
        delegate?.locationtrackDidUpdate(location)
    }
    //MARK: conform to CLLocationManagerDelegate for didFailWithError
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
       delegate?.locationtrackDidFailed(error)
    }
    
    //MARK: start update locations
    func startUpdatingMyLocation() {
        print("Starting Location Updates")
        self.mylocation?.startUpdatingLocation()
    }
    
     //MARK: stop update locations
    func stopUpdatingMyLocation() {
        print("Stop Location Updates")
        self.mylocation?.stopUpdatingLocation()
    }
    
    //Mark: addAnotation
    func addAnotation(anyObj:AnyObject,annotationArray:NSArray,pinTypes:PinTypes ,btnType:ButtonType){
        self.mapView = anyObj.mapView
        self.mapView!.mapType = MKMapType.Hybrid
        self.mapView!.showsUserLocation=true
        self.mapView?.delegate = self
        self.pinsType = pinTypes
        self.ButtonKind = btnType
        //Trigger the delegate method
        mapDelegate?.addAnnotionFromArray(self.mapView!, array: annotationArray)
       
        
       
    }

    
}

extension UserLocation: MKMapViewDelegate{
    
    //In this extension override the other delegate methdos based on requirements
    
    //MARK: conform to MKMapViewDelegate
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        //Define our reuse indentifier.
        let identifier : String = "MyAnnotion"
        var annotationView =  self.mapView! .dequeueReusableAnnotationViewWithIdentifier(identifier)
        
        if annotation.isKindOfClass(MyAnnotion) {
             let an = annotation as! MyAnnotion
            switch self.pinsType {
            case .CustomPin:
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                
                if an.imageName!.characters.count > 0
                {
                    annotationView?.image = UIImage(named: an.imageName!)
                }

            case .DefaultPin:
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            case .PinWithLeftAccesorView :
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            case .ThumbPin :
                annotationView = ThumbnailAnnotationView(identifire: identifier, annotation: an)
                return annotationView
            }
            if self.hasLeftAccesoryView {
                let an = annotation as! MyAnnotion
                annotationView?.leftCalloutAccessoryView = UIImageView(image: UIImage(named: an.leftAccesoryImageName!))
            }
            if self.hasRighAccesoryView {
                
                var btn = UIButton()
                switch self.ButtonKind {
                case .DetailDisclosure:
                    btn = UIButton(type: .DetailDisclosure)
                case .InfoLight:
                     btn = UIButton(type: .InfoLight)
                case .InfoDark:
                    btn = UIButton(type: .InfoDark)
                case .ContactAdd:
                    btn = UIButton(type: .ContactAdd)
                case .Custom:
                    //let an = annotation as! MyAnnotion
                    //let btn   = UIButton(type: UIButtonType.Custom) as UIButton
                   // btn.setImage(UIImage(named: an.rightAccesoryImageName!), forState: .Normal)
                     btn = UIButton(type: .DetailDisclosure)
                }
                //let btn = UIButton(type: .DetailDisclosure)
                annotationView?.rightCalloutAccessoryView = btn
            }
            
            annotationView?.enabled = true
            annotationView?.canShowCallout = true
            return annotationView
        }
        return annotationView;
    }
    
    func mapView(mapView: MKMapView, didSelectAnnotationView view: MKAnnotationView) {
        
        guard let customView = view as? ThumbnailAnnotationView else {
            // doesn't match
            return
        }
        customView.didSelectAnnotationViewInMap(mapView)
        
        
    }
    
    func mapView(mapView: MKMapView, didDeselectAnnotationView view: MKAnnotationView) {
        guard let customView = view as? ThumbnailAnnotationView else {
            // doesn't match
            return
        }
        customView.didDeSelectAnnotationViewInMap(mapView)
    }
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blueColor()
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        
        return polylineRenderer
        
    }

   
}



