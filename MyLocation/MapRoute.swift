//
//  MapRoute.swift
//  MyLocation
//
//  Created by Sanjay on 30/05/16.
//  Copyright © 2016 Sanjay. All rights reserved.
//

import Foundation
import MapKit

class MapRoute : NSObject, MKMapViewDelegate{
    var directionRequest: MKDirectionsRequest?
    var direction : MKDirections?
    var mapView : MKMapView?
    var srcMapItem : MKMapItem?
    var lineColor : UIColor?
    var lineWidth : CGFloat?
    init(Request:MKDirectionsRequest, Obj:AnyObject, color:UIColor, width:CGFloat) {
        super.init()
        self.directionRequest = Request
        self.direction = MKDirections(request: self.directionRequest!)
        self.mapView = Obj.mapView
        self.lineColor = color
        self.lineWidth = width
    }
    
    func  drawRoute(){
        //First draw the pins of source and destination
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.coordinate = (self.directionRequest?.source?.placemark.coordinate)!
        sourceAnnotation.title = "Source"
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.coordinate = (self.directionRequest?.destination?.placemark.coordinate)!
        destinationAnnotation.title = "destination"
        self.mapView!.showAnnotations([sourceAnnotation,destinationAnnotation], animated:true)
        self.direction!.calculateDirectionsWithCompletionHandler{
            response, error in
            guard let response = response else {
                //handle the error here
                return
            }
            let routes : NSArray = response.routes
            routes.enumerateObjectsUsingBlock({ (obj, index, stop) in
                let route = obj as! MKRoute
                self.mapView!.addOverlay(route.polyline)
                //print("route name \(route.name)")
                //print("route distance \(route.distance)")
                
                let steps = route.steps as NSArray
                steps.enumerateObjectsUsingBlock({ (obj, index, stop) in
                  let routeStep = obj as! MKRouteStep
                   //print("route instruction \(routeStep.instructions)")
                   // print("route distance \(routeStep.distance)")
                    
                })
                
            })
            
            let rect = routes[0].polyline.boundingMapRect
            self.mapView!.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }}
    
    func removeRoutes()  {
        self.mapView?.removeOverlays((self.mapView?.overlays)!)
    }
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = self.lineColor
            polylineRenderer.lineWidth = self.lineWidth!
            return polylineRenderer
        }
        
        return polylineRenderer
        
    }

}