//
//  MapAnnotation.swift
//  MyLocation
//
//  Created by Sanjay on 09/05/16.
//  Copyright © 2016 Sanjay. All rights reserved.
//

import Foundation
import MapKit
class MyAnnotion: NSObject, MKAnnotation {
    
    var title: String?
    var subtitle: String?
    var placeId: String?
    var coordinate: CLLocationCoordinate2D //This property is not optional
    var imageName : String?
    var leftAccesoryImageName : String?
    var rightAccesoryImageName : String?
    init(title:String, subtitle:String , coordinate:CLLocationCoordinate2D , placeId:String,imageName:String ,leftAccesoryImageName:String,rightAccesoryImageName:String) {
        self.title = title
        self.subtitle = subtitle
        self.placeId = placeId
        self.coordinate = coordinate
        self.imageName = imageName
        self.leftAccesoryImageName = leftAccesoryImageName
        self.rightAccesoryImageName = rightAccesoryImageName
    }
      
}

